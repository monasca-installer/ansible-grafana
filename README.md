# Grafana

Following ansible role installs Grafana3 using supported installations methods:
* RPM

## Variables

### Flow control

**run_mode** variable allows you to control what role does in the given run.
Following list provides an overview over supported run modes:

** Deploy - **default**, includes: *Stop*, *Install*, *Configure*, *Start*, *Status*
** Stop - stops the Grafana service
** Install - installs Grafana service based on supplied installation method
** Configure - excutes configuration phase for the service
** Start - starts the service
** Status - verifies if Grafana server has been been actually started (is the port taken ?)
** Purge - removes Grafana from the system. Includes *Stop*

### Mandatory variables

| Name | Default | Description |
|------|---------|-------------|
| `grafana_install_package_name` | grafana | Name of the grafana package |
| `grafana_install_mode` | rpm | Currently supported only direct RPM installation. But it is possible to have it done using, for instance, also DEB package |
| `grafana_install_rpm_download_url` | '' | Required to be set if {{ grafana_install_mode}} == 'rpm' |
| `grafana_install_version`| 3.0 | Version of installed Grafana |
| `grafana_wait_for_port_time` | 15 | How long should wait_for module wait for `grafana_server_http_port` to be taken by grafana |

### Other variables

| Name | Default | Description |
|------|---------|-------------|
| `grafana_app_mode`| development | Possible values : *production*, *development* |
| `grafana_instance_name` | `grafana.{{ ansible_fqdn }}` | Instance name, default to FGQN of the machine |
| `grafana_plugins` | [] | Lists of grafana plugins. For details refer to **Plugins** section |

#### Paths

| Name                    | Default          | Description                                                                                |
| ------                  | ---------        | -------------                                                                              |
| `grafana_paths_data`    | /var/grafana     | Path to where grafana can store temp files, sessions, and the sqlite3 db (if that is used) |
| `grafana_paths_logs`    | /var/log/grafana | Directory where grafana can store logs                                                     |
| `grafana_paths_plugins` | /var/lib/grafana | Directory where grafana will automatically scan and look for plugins                       |
| `grafana_path_home`     | /opt/grafana     | Home directory for Grafana                                                                 |
| `grafana_paths_run`     | /var/run/grafana | Run-time variable data directory for Grafana                                               |

#### Server

| Name | Default | Description |
|------|---------|-------------|
| `grafana_server_protocol` | http | Protocol (http or https) |
| `grafana_server_http_addr` | {{ ansible_ssh_hostname }} | The ip address to bind to, empty will bind to all interfaces |
| `grafana_server_http_port` | 3000 | The http port  to use |
| `grafana_server_domain` | localhost | The public facing domain name used to access grafana from a browser |
| `grafana_server_enforce_domain` | true | Redirect to correct domain if host header does not match domain. Prevents DNS rebinding attacks |
| `grafana_server_root_url` | %(protocol)s://%(domain)s:%(http_port)s/ | |
| `grafana_server_router_logging_enabled` | false | Log web requests |
| `grafana_server_static_root_path` | public | Relative working path |
| `grafana_server_gzip_enabled` | true | Is Gzip enabled for the server |
| `grafana_server_cert_file` | '' | Path to certificate |
| `grafana_server_cert_key` | '' | Key of the certificate |

#### Logging

| Name | Default | Description |
| ---- | ------- | ----------- |
| `grafana_log_mode` | file | Either *console*, *file* or *syslog*. Use comma to separate multiple modes |
| `grafana_log_level`| info | Either "debug", "info", "warn", "error", "critical", default is "info" |
| `grafana_log_format` | text | Log line format, valid options are *text*, *console* and *json* |
| `grafana_console_log_level` | `grafana_log_level` | Granular value to control console log level |
| `grafana_console_log_format` | `grafana_log_format` | Granular value to control console log format |
| `grafana_file_log_level` | `grafana_log_level` | Granular value to control file log level |
| `grafana_file_log_format` | `grafana_log_format` | Granular value to control file log format |
| `grafana_file_log_rotate` | true | This enables automated log rotate(switch of following options), default is true |
| `grafana_file_log_max_lines` | 1000000 | Max line number of single file, default is 1000000 |
| `grafana_file_log_max_size_shift` | 28 | Max size shift of single file, default is 28 means 1 << 28, 256MB |
| `grafana_file_log_daily_rotate` | true | egment log daily, default is true |
| `granafa_file_log_max_days` | 7 | Expired days of log file(delete after max days), default is 7 |
| `grafana_syslog_log_level` | `grafana_log_level` | Granular value to control syslog log level |
| `grafana_syslog_log_format` | `grafana_log_format` | Granular value to control syslog log format |
| `grafana_syslog_log_network` | '' | Either *udp*, *tcp* or *unix*. By defualt blank, which means default unix setting will be used |
| `grafana_syslog_log_address` | '' | Address to ship the logs. |
| `grafana_syslog_log_facility` | '' | Syslog facility. user, daemon and local0 through local7 are valid |
| `grafana_syslog_log_tag` | '' | Syslog tag. By default, the process' argv[0] is used |
| `run_mode` | Deploy | Available options: *Stop*, *Install*, *Configure*, *Start*, *Status*, *Deploy* |

#### Authentication

| Name | Default | Description |
| ---- | ------- | ----------- |
| `grafana_auth_basic_enabled` | true | Is basic authentication enabled |
| `grafana_auth_keystone_enabled` | false | Is keystone authentication enabled |
| `grafana_auth_keystone_auth_url` | http://127.0.0.1:5000 | Admin endpoint of keystone service |
| `grafana_auth_keystone_defualt_domain` | default | ? |
| `grafana_auth_keystone_default_role` | '' | ? |
| `grafana_auth_keystone_global_admin_roles` | [] | ? |
| `grafana_auth_keystone_admin_roles` | [admin] | Roles that are considered to mark user with admin priviliges |
| `grafana_auth_keystone_editor_roles` | [_member_] | ? |
| `grafana_auth_keystone_read_editor_roles` | [] | ? |
| `grafana_auth_keystone_viewer_roles` | []] | ? |
| `grafana_auth_keystone_verify_ssl_cert` | False | Should SSL certificate be verified. Use false to disable |
| `grafana_auth_keystone_root_ca_pem_file` | [] | Path to the certificate |

#### Users

| Name | Default | Description |
| ---- | ------- | ----------- |
| `grafana_users_allow_sign_up` | false | Should users be allowed to create accounts |
| `grafana_users_allow_org_create` | false | Should creating organizations be alllowed |
| `grafana_users_auto_assign_org` | false | Should default organization be assigned |
| `grafana_users_auto_assign_org_role` | '' | Default role that should be assigned to the user |
| `grafana_users_verify_email_enabled` | false | Is email verification enabled |
| `grafana_users_login_hint` | username | Hint presented to user during the logging in |
| `grafana_users_default_theme` | dark | Default theme. Either *dark*, *light* area allowed |
| `grafana_users_allow_user_pass_login` | true | Allow users to sign in using username and password |

#### Database

| Name | Default | Description |
| ---- | ------- | ----------- |
| `grafana_database_type` | mysql | Database type to use. Permitted are *mysql*, *postgres* and *sqlite3*, however only *mysql* is supported |
| `grafana_database_host` | 127.0.0.1:3306 | Address where database is accessible. Relevant only for *mysql* and *postgres* |
| `grafana_database_name` | grafana | Name of the database grafana should use |
| `grafana_database_user` | grafana | User name that can access grafana database |
| `grafana_database_password` | grafana | Password for `grafana_database_user` |
| `grafana_database_ssl_mode` | false | Depends on `grafana_database_type`. For *mysql* it is one of [true, false, skip-verify]. For *postgres* it is [disable, require, verify-full] |

### Read-only variables

| Name | Value | `ansible_os_family` | Description |
| ---- | ----- | ------------------- | ---------   |
| `grafana_service_name` | grafana-server | N/A | Name of the service as installed by RPM package |
| `grafana_ini_file` | {{ grafana_paths_config }}/grafana.ini | N/A | Grafana configuration file |
| `grafana_rpm_dependencies` | ... | RedHat | List of additional dependencies for RPM package, check [here](vars/RedHat.yml) |
| `grafana_environment_file` | /etc/sysconfig/grafana-server | RedHat | File is specified inside the service file installed with RPM |
| `grafana_rpm_user` | grafana | RedHat | Grafana user, equal to the one supplied with RPM package |
| `grafana_rpm_group` | grafana | RedHat | Grafana group, equal to the one supplied with RPM package |
| `grafana_purge_items` | ... | N/A | List of resources to remove from the file system during purge |
| `grafana_extra_purge_items` | ... | N/A | List of extra items to purge. That variable is OS specific |

## Plugins

List of plugins that should be installed along with Grafana can be specified with *grafana_plugins* variable.
This must be an array of items having specific format. An example can be found below:

```yml
grafana_plugins:
  - name: plugin-name
    url: http://example.com/plugin-name.tar
```

Explanation:
* name - will be used as directory name inside Grafana plugins directory
* url: public URL from which plugin will be download

# License

Apache License, Version 2.0

## Author Information

Tomasz Trebski
